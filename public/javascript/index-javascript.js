//METODO GET PARA MOSTRAR LA LISTA
metodoGet = async () => {
  const response = await fetch("http://localhost:8080/api/get", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });
  const data = await response.json();
  console.log(data);
  const tableBody = document.getElementById("tableBody");
  let clase;
  //CREA ROWS
  for (var i = 0; i < data.prospectos.length; i++) {
    const prospecto = data.prospectos[i];
    let row = document.createElement("tr");
    row.innerHTML += "<th>" + prospecto.id + "</th>";
    row.innerHTML += "<td>" + prospecto.nombre + "</td>";
    row.innerHTML += "<td>" + prospecto.apellidoP + "</td>";
    row.innerHTML += "<td>" + prospecto.apellidoM + "</td>";
    clase = prospecto.estatus == "Enviado" ? "status-send" : prospecto.estatus == "Rechazado" ? "status-reject" : "status-success";
    row.innerHTML += `<td  class="${clase}">  ${prospecto.estatus}  </td>`;
    row.innerHTML += `<td class='align-text'><button ${prospecto.estatus == "Rechazado" ? "disabled" : null} onclick="metodoGetByIdEvaluar(value)" value= "${prospecto.id}" type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#modalEvaluate"><i class="bi-person-square"></i></button></td>`;
    row.innerHTML += `<td class='align-text'><button onclick="metodoGetById(value)" value= "${prospecto.id}" type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#modalView"><i class="bi-eye-fill m-1"></i>Ver</button></td>`;
    tableBody.appendChild(row);
  }
};
//MOSTRAR POR ID
metodoGetById = async (id) => {
  const response = await fetch("http://localhost:8080/api/get/" + id, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });

  const responseArchivo = await fetch(
    "http://localhost:8080/api/archivos/" + id, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });

  //SI EL ESTATUS ES 200 LLENAMOS EL FORMULARIO DEL ID ESPECIFICADO
  if (response.status == 200) {
    const data = await response.json();
    let datoID = data.id;
    let estatus = data.estatus;
    document.getElementById("prospectoID").innerHTML =
      "Prospecto # " + datoID + " | ESTATUS: " + estatus;
    let nombre = document.getElementById("nombre");
    let apellidoP = document.getElementById("apellidoP");
    let apellidoM = document.getElementById("apellidoM");
    let direccion = document.getElementById("direccion");
    let rfc = document.getElementById("rfc");
    let numero = document.getElementById("numero");
    let colonia = document.getElementById("colonia");
    let codigoPostal = document.getElementById("codigoPostal");
    let telefono = document.getElementById("telefono");

    nombre.value = data.nombre;
    apellidoP.value = data.apellidoP;
    apellidoM.value = data.apellidoM;
    direccion.value = data.calle;
    rfc.value = data.rfc;
    numero.value = data.numero;
    colonia.value = data.colonia;
    codigoPostal.value = data.codigoPostal;
    telefono.value = data.telefono;
    const divTextArea = document.getElementById("divTextArea");

    if (divTextArea) {
      document.getElementById("contenedorForm").removeChild(divTextArea);
    }
    //SI ES RECHAZADO CREAMOS EL AREA DEL MOTIVO DE RECHAZO
    if (data.estatus == "Rechazado") {
      const contenedorForm = document.getElementById("contenedorForm");
      let textArea = document.createElement("div");
      textArea.setAttribute("id", "divTextArea");
      textArea.innerHTML += `<label class="form-label">Motivo del rechazo</label>
      <textarea class="form-control" id="motivo" rows="3" disabled></textarea>`;
      contenedorForm.appendChild(textArea);

      let motivo = document.getElementById("motivo");
      motivo.value = data.observaciones;
    }
  }

  if (responseArchivo.status == 200) {
    const data = await responseArchivo.json();
    console.log(data);
    const archivos = document.getElementById("archivos");
    archivos.innerText = "";
    for (let i = 0; i < data.archivos.length; i++) {
      let elementA = document.createElement("a");
      elementA.setAttribute("href", data.archivos[i].ruta);
      elementA.setAttribute("target", "_blank");
      elementA.setAttribute("id", "rutaArchivo");
      elementA.innerText = data.archivos[i].ruta.split("/")[2];
      archivos.appendChild(elementA);
    }
  }
};

//METODO PARA ACTUALIZAR EL ESTATUS DEL USUARIO
metodoPut = async (id, estatus, observaciones) => {
  const response = await fetch("http://localhost:8080/api/put/" + id, {
    method: "PUT",
    body: JSON.stringify({ estatus, observaciones }),
    headers: {
      "Content-Type": "application/json",
    },
  });
  if (response.status == 200) {
    Swal.fire({
      icon: "success",
      title: "Prospecto " + estatus,
      showConfirmButton: false,
      timer: 1500,
      willClose: function () { document.location.href = "/index.html"; },
    });
  }
};

//METODO PARA OBTENER LA INFORMACION DEL USUARIO A EVALUAR
metodoGetByIdEvaluar = async (id) => {
  const response = await fetch("http://localhost:8080/api/get/" + id, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });

  const responseArchivo = await fetch(
    "http://localhost:8080/api/archivos/" + id, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });

  //LLENAMOS EL FORMULARIO CON LA INFO
  if (response.status == 200) {
    const data = await response.json();
    let datoID = data.id;
    let estatus = data.estatus;
    document.getElementById("prospectoIDEvaluar").innerHTML = "Prospecto # " + datoID + " | ESTATUS: " + estatus;
    let nombre = document.getElementById("nombreEvaluar");
    let apellidoP = document.getElementById("apellidoPEvaluar");
    let apellidoM = document.getElementById("apellidoMEvaluar");
    let direccion = document.getElementById("direccionEvaluar");
    let rfc = document.getElementById("rfcEvaluar");
    let numero = document.getElementById("numeroEvaluar");
    let colonia = document.getElementById("coloniaEvaluar");
    let codigoPostal = document.getElementById("codigoPostalEvaluar");
    let telefono = document.getElementById("telefonoEvaluar");

    nombre.value = data.nombre;
    apellidoP.value = data.apellidoP;
    apellidoM.value = data.apellidoM;
    direccion.value = data.calle;
    rfc.value = data.rfc;
    numero.value = data.numero;
    colonia.value = data.colonia;
    codigoPostal.value = data.codigoPostal;
    telefono.value = data.telefono;
    const divTextArea = document.getElementById("divTextArea");

    if (divTextArea) {
      document.getElementById("contenedorForm").removeChild(divTextArea);
    }

    if (data.estatus == "Rechazado") {
      const contenedorForm = document.getElementById("contenedorForm");
      let textArea = document.createElement("div");
      textArea.setAttribute("id", "divTextArea");
      textArea.innerHTML += `<label class="form-label">Motivo del rechazo</label>
        <textarea class="form-control" id="motivo" rows="3" disabled></textarea>`;
      contenedorForm.appendChild(textArea);

      let motivo = document.getElementById("motivo");
      motivo.value = data.observaciones;
    }
    //SI EL USUARIO YA ESTA AUTORIZADO DESHABILITAMOS BOTON AUTORIZAR
    if (data.estatus == "Autorizado") {
      document.getElementById("btnAutorizar").setAttribute("disabled", true);
    }
  }

  if (responseArchivo.status == 200) {
    const data = await responseArchivo.json();
    console.log(data);
    const archivos = document.getElementById("archivosEvaluar");
    archivos.innerText = "";
    for (let i = 0; i < data.archivos.length; i++) {
      let elementA = document.createElement("a");
      elementA.setAttribute("href", data.archivos[i].ruta);
      elementA.setAttribute("target", "_blank");
      elementA.innerText = data.archivos[i].ruta.split("/")[2];
      archivos.appendChild(elementA);
    }
  }
  //FUNCION DEL BOTON AUTORIZAR
  let btnAutorizar = document.getElementById("btnAutorizar");
  btnAutorizar.addEventListener("click", async function () {
    await metodoPut(id, "Autorizado");
  });
  //FUNCION DESPUES DE RECHAZAR
  let btnEnviar = document.getElementById("btnEnviar");
  btnEnviar.addEventListener("click", async function () {
    let txtObservaciones = document.getElementById("txtObservaciones").value;
    await metodoPut(id, "Rechazado", txtObservaciones);
  });

};
window.onload = metodoGet();
