let URLPOST = "http://localhost:8080/api/post";

// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
  "use strict";

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.querySelectorAll(".needs-validation");

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms).forEach(function (form) {
    form.addEventListener(
      "submit",
      async function (event) {
        event.preventDefault();
        event.stopPropagation();
        if (form.checkValidity()) {
          const form = document.getElementById("forma");

          //OBTENEMOS LOS DATOS DEL FORM A formData
          let formData = new FormData(form);
          formData.append("estatus", "Enviado");
          formData.append("pathArchivo", "esto es un path");

          await fetch(URLPOST, {
            method: "POST",
            body: formData,
            cache: "no-cache",
          }).then((response) => {
            console.log(response);
            if (response.status == 200) {
              Swal.fire({
                icon: "success",
                title: "Prospecto guardado",
                showConfirmButton: false,
                timer: 1500,
                willClose: function(){document.location.href = "/index.html";},
                
              });
            }
          });
        }
        form.classList.add("was-validated");
      },
      false
    );
  });
})();

//Solo numeros
function isNumberKey(evt) {
  var charCode = evt.which ? evt.which : evt.keyCode;
  if ((charCode = 46 && charCode > 31 && (charCode < 48 || charCode > 57)))
    return false;
  return true;
}
function isNumericKey(evt) {
  var charCode = evt.which ? evt.which : evt.keyCode;
  if ((charCode = 46 && charCode > 31 && (charCode < 48 || charCode > 57)))
    return true;
  return false;
}
