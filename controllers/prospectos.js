const Prospecto = require("../model/prospectos-schema");
const Archivo = require('../model/archivos-schema');

//OBTENER PROSPECTOS
const getProspectos = async (req, res) => {
  const prospectos = await Prospecto.findAll();
  res.json({ prospectos });
};

//OBTENER PROSPECTOS POR ID
const getProspectoID = async (req, res) => {
  const { id } = req.params;
  const prospectoID = await Prospecto.findByPk(id);
  if (prospectoID) {
    res.json(prospectoID);
  } else {
    res.status(404).json({
      msg: `No se encuentra el usuaro con el id ${id}`,
    });
  }
};

//CAPTURAR PROSPECTOS
const postProspecto = async (req, res) => {
  const {body, files} = req;
  try{
    const prospecto = new Prospecto(body);
    const save = await prospecto.save();
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      const archivo = new Archivo({idProspecto:save.dataValues.id, ruta: "/uploads/"+file.originalname});
      await archivo.save();
    }
    res.json(prospecto);
  }
  catch(error){
    console.log(error);
    res.status(500).json({
      msg: 'algo salio mal'
    });
  }
};

//ACTUALIZAR ESTADO PROSPECTO Y OBSERVACION SI ES RECHAZADO
const putProspecto = async (req, res) => {
  const { id } = req.params;
  const { estatus, observaciones } = req.body;

  try {
    const prospecto = await Prospecto.findByPk(id);
    if (!prospecto) {
      return res.status(400).json({
        msg: `No se encuentra el usuaro con el id ${id}`,
      });
    }
    await prospecto.update({ estatus, observaciones });
    res.json(prospecto);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Ocurrio un error",
    });
  }
};

module.exports = { getProspectos, getProspectoID, postProspecto, putProspecto };
