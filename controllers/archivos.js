const Archivo = require('../model/archivos-schema');

const getArchivos = async (req, res) => {
  const {id} = req.params;
  const archivos = await Archivo.findAll({where:{idProspecto:id}});
  res.json({ archivos });
};

module.exports = {getArchivos};