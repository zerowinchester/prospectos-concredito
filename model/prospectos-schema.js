const { DataTypes } = require("sequelize");
const db = require("../database/connection.js");

const Prospectos = db.define("prospectos", {
  nombre: {
    type: DataTypes.STRING,
  },
  apellidoP: {
    type: DataTypes.STRING,
  },
  apellidoM: {
    type: DataTypes.STRING,
  },
  calle: {
    type: DataTypes.STRING,
  },
  numero: {
    type: DataTypes.STRING,
  },
  colonia: {
    type: DataTypes.STRING,
  },
  codigoPostal: {
    type: DataTypes.STRING,
  },
  telefono: {
    type: DataTypes.STRING,
  },
  rfc: {
    type: DataTypes.STRING,
  },
  estatus: {
    type: DataTypes.STRING,
  },
  observaciones: {
    type: DataTypes.STRING,
  },
});


module.exports = Prospectos;