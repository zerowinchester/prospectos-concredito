const { DataTypes } = require("sequelize");
const db = require("../database/connection.js");
const Archivo = db.define("archivos", {
    idProspecto: {
      type: DataTypes.INTEGER,
    },
    ruta: {
      type: DataTypes.STRING,
    },
  });

  module.exports = Archivo;