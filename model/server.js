const express = require('express');
const cors = require('cors');
const db = require('../database/connection.js');
const multer = require("multer");
const path = require("path");

//STORAGE PARA GUARDAR ARCHIVOS
const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, path.join(__dirname, "../public/uploads"));
    },
    filename:function(req, file, cb){

        cb(null, file.originalname);
    }
});

class server{
    constructor(){
        this.app = express();
        this.port = 8080;
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended:true}));
        this.app.use(multer({storage}).array("formFileMultiple"));
        this.middleware();
        this.route();        
        this.dbConnection();
    }

    listen(){
        this.app.listen(this.port, () =>{
            console.log("Servidor corriendo en puerto: " + this.port);
        });
    }

    route(){
        this.app.use("/api/", require('../routes/routes.js'));
        this.app.use("/api/archivos/", require("../routes/routeArchivo.js"));
    }

    middleware(){
        this.app.use(cors());
        this.app.use(express.static('public'));
    }

    async dbConnection() {
        try {
            await db.authenticate();
            console.log('Database online');
        } catch (error) {
            throw new Error( error );
        }
    }
}

module.exports = server;