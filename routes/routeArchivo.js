const { Router } = require('express');
const {getArchivos} = require('../controllers/archivos');


const router = Router();

router.get("/:id", getArchivos);



module.exports = router;