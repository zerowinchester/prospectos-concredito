const { Router } = require('express');
const { getProspectos, getProspectoID, postProspecto, putProspecto } = require('../controllers/prospectos');


const router = Router();

router.get("/get", getProspectos);

router.get("/get/:id", getProspectoID);

router.post("/post", postProspecto);

router.put("/put/:id", putProspecto);

module.exports = router;